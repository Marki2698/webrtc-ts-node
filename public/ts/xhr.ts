const xhr: XMLHttpRequest = new XMLHttpRequest();

export default {
    init(url: string, method: string, data: Object, cb: (message: string) => void) {
        xhr.open(method, url, true);
        xhr.onreadystatechange = () => {
            if(xhr.status === 200 && xhr.readyState === 4) {
                cb.call(null, xhr.responseText);
            } else if(xhr.status === 500) {
                cb.call(null, xhr.responseText);
            } else {
                cb.call(null, 'sending');
            }
        };
        xhr.send(JSON.stringify(data));
    }
};