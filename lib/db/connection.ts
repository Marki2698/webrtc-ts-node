import mongoose from "mongoose";
import config from "../config";
import { MongoError } from "mongodb";

export default () => {
    mongoose.connect(config.url, {
        useNewUrlParser: true
    }).then((fullfill) => console.log('connected'), (reason) => console.log(reason));
};