import express from "express";
import router from "./lib/routes/public";
import bodyParser from "body-parser";
import connection from "./lib/db/connection";

const app = express();

app.use(bodyParser.urlencoded({
    extended: false
}));
connection(); // establish connection
app.use(router);
app.use(express.static('public'));

//console.log(process.env);
const port: number | string = process.env.PORT || 3000;

app.listen(port, () => {
    console.log(`listen on ${port} port`);
});