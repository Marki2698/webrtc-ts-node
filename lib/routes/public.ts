import express from "express";

const router = express.Router();

router.get('/', (req, res, next) => {
    res.sendFile(`${process.cwd()}/public/html/index.html`);
    //res.send('hello from express');
});

export default router;