import AJAX from "./xhr";

interface SignUpObject {
    email: string,
    name: string,
    password: string,
    word: string
};

interface SignInObject {
    email: string,
    password: string
};

interface ISign {
    Listener(elem: string): void;
    PrepareData(signup: boolean): SignUpObject | SignInObject | boolean;
    Sign(data: SignInObject | SignUpObject): void;
}

class Sign implements ISign {
    constructor(){};
    Listener(elem: string): void {
        const el = document.querySelector(elem);
        el.addEventListener('click', (ev) => {
            console.log(ev.target);
            let data;
            if(ev.target.className === 'signup-btn') {
                data = this.PrepareData(true);
            } else if(ev.target.className === 'signin-btn') {
                data = this.PrepareData(false); 
            }
        });
    }
    PrepareData(signup: boolean): SignUpObject| SignInObject | boolean {
        let data: Object = {}; 
        if(signup) {
            data.email
        }
    }
    Sign(data: SignUpObject | SignInObject): void {

    }
}

const button = document.getElementsByTagName('button');
